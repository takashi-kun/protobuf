namespace Proto.Editor
{
    using UnityEngine;

    public class ProtoCreation
    {
        public string protoPackage;
        
        public string csharpNamespace;

        public bool   javaMultipleFiles;
        public string javaPackage;
        public string storeLocation;

        public ProtoCreation()
        {
            protoPackage = "template";

            csharpNamespace = Application.identifier;

            javaMultipleFiles = true;
            javaPackage       = "com.protobuf.model";
            storeLocation     = Application.dataPath;
        }
    }

    public class ProtoExporter
    {
        public string protoPath;
        public string outputPath;

        public ProtoExporter()
        {
            protoPath  = Application.dataPath;
            outputPath = Application.dataPath;
        }
    }
}