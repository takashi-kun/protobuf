namespace Proto.Editor
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using UnityEditor;
    using UnityEngine;

    public class ProtoExportEditor : EditorWindow
    {
        [MenuItem("Proto/Exporter Editor")]
        private static void Run()
        {
            var window = GetWindow(typeof(ProtoExportEditor));
            window.titleContent.text = "Google Proto Exporter";
            window.minSize           = new Vector2(600, 500);
            window.maxSize           = new Vector2(600, 500);
        }
        
        
        private ProtoExporter _exporter;

        private string _selectedFile;
        private int    _selectedFileIndex;
        private int    _selectedLanguageIndex;

        private Vector2       _scrollPosition;
        private StringBuilder _resultText;

        private string[] _supportedLanguages = new[] { "C#", "Java" };

        private GUIStyle _headerStyle;
        private GUIStyle _style;

        private static readonly string AllItems = "All Items";
        private static readonly string NotFound = "Proto Files Not Found";

        private static readonly string ExecutableFilePath = @"/Proto/Editor/Tools/protoc.exe";
        private static readonly string CSharpOutCommand   = @"--csharp_out=";
        private static readonly string JavaOutCommand     = @"--java_out=";
        private static readonly string ProtoPathCommand   = @"--proto_path=";
        
        private static readonly string ProtoExporterKey   = @"exporter_key";

        private void OnEnable()
        {
            _exporter = ProtoEditorServices.GetStoredObject<ProtoExporter>(ProtoExporterKey);

            _resultText = new StringBuilder();
        }

        private void InitStyles()
        {
            if (_headerStyle == null)
            {
                _headerStyle                  = new GUIStyle(GUI.skin.label);
                _headerStyle.fontStyle        = FontStyle.Bold;
                _headerStyle.normal.textColor = Color.cyan;
            }
            
            if (_style == null)
            {
                _style                  = new GUIStyle(GUI.skin.textArea);
                _style.margin           = new RectOffset(5, 5, 5, 5);
                _style.fontStyle        = FontStyle.Italic;
                _style.wordWrap         = true;
                _style.normal.textColor = Color.white;
                _style.richText         = true;
            }
        }


        private void OnGUI()
        {
            InitStyles();

            EditorGUILayout.BeginVertical("box");
            EditorGUILayout.Space(15);
            _exporter.protoPath = DrawBrowsePath("Proto Path", _exporter.protoPath, "Select Directory of Proto Files");
            
            EditorGUILayout.Space(15);
            _exporter.outputPath = DrawBrowsePath("Output Path", _exporter.outputPath, "Select Directory to Export");
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.Space(10);
            DrawStuff();
            
            EditorGUILayout.Space(10);
            DrawExecute();
        }

        private string DrawBrowsePath(string welcomeText, string variable, string browseTitle)
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button(welcomeText, _headerStyle))
            {
                if (Directory.Exists(variable))
                {
                    Application.OpenURL(variable);
                }
                else
                {
                    Application.OpenURL(Application.dataPath);
                }
            }

            string pathAsString = GUILayout.TextField(variable, GUILayout.Width(500));

            EditorGUILayout.EndHorizontal();
            GUILayout.Space(5);
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Browse", GUILayout.Width(100)))
            {
                string selectedPath = EditorUtility.OpenFolderPanel(browseTitle, variable, "");
                if (!string.IsNullOrEmpty(selectedPath))
                {
                    pathAsString = selectedPath;
                }
                
                // remove all focus
                GUI.FocusControl(null);
            }
            
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            return pathAsString;
        }

        private void DrawStuff()
        {
            EditorGUILayout.BeginVertical("box");
            DrawSelectedOne();
            EditorGUILayout.Space(15);
            DrawLanguageOptions();
            EditorGUILayout.EndVertical();
        }

        private void DrawSelectedOne()
        {
            string[] allFiles = FindAllProtoFiles();
            string[] options  = new string[allFiles.Length + 1];
            options[0] = allFiles.Length == 0 ? NotFound : AllItems;

            for (int i = 0; i < allFiles.Length; i++)
            {
                options[i + 1] = allFiles[i];
            }

            _selectedFileIndex = EditorGUILayout.Popup("Selected File: ", _selectedFileIndex, options, GUILayout.Width(400));
            if (_selectedFileIndex >= 0)
            {
                _selectedFile = options[_selectedFileIndex];
            }
            else
            {
                _selectedFile = options[0];
            }
        }

        private string[] FindAllProtoFiles()
        {
            if (Directory.Exists(_exporter.protoPath))
            {
                var info  = new DirectoryInfo(_exporter.protoPath);
                var files = info.GetFiles("*.proto", SearchOption.TopDirectoryOnly);
                if (files.Length > 0)
                {
                    var list = new string[files.Length];
                    for (int i = 0; i < files.Length; i++)
                    {
                        list[i] = files[i].Name;
                    }

                    return list;
                }
            }

            return new string[] {};
        }

        private void DrawLanguageOptions()
        {
            _selectedLanguageIndex = EditorGUILayout.Popup("Export in: ", _selectedLanguageIndex, _supportedLanguages, GUILayout.Width(250));
        }

        private void DrawExecute()
        {
            EditorGUILayout.BeginVertical();
            
            GUILayout.Label("Log");
            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition,GUILayout.Height(200));
            
            EditorGUILayout.TextArea(_resultText.ToString(), _style, GUILayout.ExpandHeight(true));
            EditorGUILayout.EndScrollView();
            
            EditorGUILayout.Space(5);

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Execute", GUILayout.Width(100), GUILayout.Height(40)))
            {
                Save();
                Execute();
                
                // remove all focus
                GUI.FocusControl(null);
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        private void Execute()
        {
            string outParam      = _selectedLanguageIndex == 0 ? CSharpOutCommand : JavaOutCommand;
            string finalOutParam = outParam + _exporter.outputPath.Trim();

            string finalProtoPathCommand = ProtoPathCommand + _exporter.protoPath.Trim();

            if (_selectedFile.Equals(NotFound))
            {
                Log($"<color=yellow>No proto files was found in {_exporter.protoPath}! Try again with another path.</color>");
                return;
            }
            
            Log("Start executing...");
            if (_selectedFile.Equals(AllItems))
            {
                ExecuteCommand(finalOutParam, finalProtoPathCommand, FindAllProtoFiles());
            }
            else
            {
                ExecuteCommand(finalOutParam, finalProtoPathCommand, new []{ _selectedFile });
            }
            
            Print("\n----------------------------------------------\n");
        }

        private void ExecuteCommand(string outCommand, string protoCommand, string[] listFiles)
        {
            var path = Application.dataPath + ExecutableFilePath;
            if (!File.Exists(path))
            {
                Log($"<color=red>Executable file 'protoc.exe' not found in {path}. Abort!</color>");
                return;
            }

            using (Process p = new Process())
            {
                ProcessStartInfo psi = new ProcessStartInfo(path);
                psi.CreateNoWindow         = true;
                psi.UseShellExecute        = false;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError  = true;

                string preCommandArgs = outCommand + " " + protoCommand + " ";

                for (int i = 0; i < listFiles.Length; i++)
                {
                    psi.Arguments = preCommandArgs + listFiles[i];
                    p.StartInfo   = psi;
                    p.Start();
                    
                    p.WaitForExit();
                    
                    if (p.ExitCode == 1)
                    {
                        Print($"\nError was found in {listFiles[i]}. Log below: ");
                        
                        StreamReader error = p.StandardError;
                        while (error.Peek() > -1)
                        {
                            Print("<color=red>" + error.ReadLine() + "</color>");
                        }
                        
                        Print("---- End of log ----\n");
                    }
                }
                
                Log($"<color=cyan>{listFiles.Length.ToString()} proto file(s) was executed in {_supportedLanguages[_selectedLanguageIndex]}. Finish!</color>");
            }
        }

        private void Log(string content)
        {
            string nowAsString = DateTime.Now.ToString("[HH:mm:ss]: ");
            _resultText.Append(nowAsString);
            _resultText.Append(content);
            _resultText.Append("\n");
        }

        private void Print(string content)
        {
            _resultText.Append(content);
            _resultText.Append("\n");
        }

        private void Save()
        {
            ProtoEditorServices.StoreObject(ProtoExporterKey, _exporter);
        }
    }
}