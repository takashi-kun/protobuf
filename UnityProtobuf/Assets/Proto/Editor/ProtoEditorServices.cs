namespace Proto.Editor
{
    using UnityEditor;
    using UnityEngine;

    public static class ProtoEditorServices
    {
        public static T GetStoredObject<T>(string key) where T : class, new()
        {
            if (EditorPrefs.HasKey(key))
            {
                string json = EditorPrefs.GetString(key);
                return JsonUtility.FromJson<T>(json);
            }

            return new T();
        }

        public static void StoreObject<T>(string key, T t) where T : class, new()
        {
            string json = JsonUtility.ToJson(t);
            EditorPrefs.SetString(key, json);
        }
    }
}