namespace Proto.Editor
{
    using System;
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    public class ProtoCreationEditor : EditorWindow
    {
        [MenuItem("Proto/Create Message", priority = 101)]
        private static void Run()
        {
            var window = GetWindow(typeof(ProtoCreationEditor));
            window.titleContent.text = "Create Protobuf Message";
            window.minSize           = new Vector2(600, 480);
            window.maxSize           = new Vector2(600, 480);
        }


        private string _fileName;

        private ProtoCreation _creation;

        private GUIStyle _headerStyle;
        private GUIStyle _errorStyle;
        private GUIStyle _warningStyle;

        private static readonly string CreationKey = "proto_creation";

        private static readonly string ProtoPackageBody = @"package **Package**;";
        private static readonly string ProtoOptionBody = @"option **OptionParam** = ""**OptionValue**"";";

        private static readonly string MessageBody =
            @"
syntax = ""proto3"";

**ProtoPackage**

**CSharp**

**Java**

message **ClassName** 
{
    /**
    Your properties goes here
    See document at: https://developers.google.com/protocol-buffers/docs/proto3#simple
    **/
}
";

        private void OnEnable()
        {
            _fileName       = "your_file_name";
            _creation       = ProtoEditorServices.GetStoredObject<ProtoCreation>(CreationKey);
        }

        private void OnGUI()
        {
            InitStyles();

            EditorGUILayout.Space(10);
            DrawGeneral();

            EditorGUILayout.Space(10);
            DrawCSharp();

            EditorGUILayout.Space(10);
            DrawJava();

            EditorGUILayout.Space(15);
            DrawBrowsePath();

            EditorGUILayout.Space(10);
            DrawExecute();
        }

        private void InitStyles()
        {
            if (_headerStyle == null)
            {
                _headerStyle                  = new GUIStyle(GUI.skin.label);
                _headerStyle.fontStyle        = FontStyle.Bold;
                _headerStyle.normal.textColor = Color.cyan;
            }
            
            if (_errorStyle == null)
            {
                _errorStyle                  = new GUIStyle(GUI.skin.label);
                _errorStyle.fontStyle        = FontStyle.Italic;
                _errorStyle.fontSize         = 12;
                _errorStyle.normal.textColor = Color.red;
            }
            
            if (_warningStyle == null)
            {
                _warningStyle                  = new GUIStyle(GUI.skin.label);
                _warningStyle.fontStyle        = FontStyle.Italic;
                _warningStyle.fontSize         = 12;
                _warningStyle.normal.textColor = Color.yellow;
            }
        }

        private void DrawGeneral()
        {
            if (GUILayout.Button("General", _headerStyle))
            {
                Application.OpenURL("https://developers.google.com/protocol-buffers/docs/overview#syntax");
            }
            
            EditorGUILayout.BeginVertical("box");

            _fileName = DrawTextField("File Name*", _fileName);
            if (string.IsNullOrEmpty(_fileName))
            {
                GUILayout.Label("* This field is required", _errorStyle);
            }

            _creation.protoPackage = DrawTextField("Proto Package", _creation.protoPackage);
            if (string.IsNullOrEmpty(_creation.protoPackage))
            {
                if (string.IsNullOrEmpty(_creation.csharpNamespace))
                {
                    GUILayout.Label("Warning: C# export file will have no Namespace at all", _warningStyle);
                }

                if (string.IsNullOrEmpty(_creation.javaPackage))
                {
                    GUILayout.Label("Warning: Java export files will not have Package", _warningStyle);
                }
            }

            EditorGUILayout.EndVertical();
        }


        private string DrawTextField(string titleField, string variable)
        {
            EditorGUILayout.Space(5);
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label(titleField);
            string text = GUILayout.TextField(variable, GUILayout.Width(435));

            EditorGUILayout.EndHorizontal();

            return text;
        }

        private void DrawCSharp()
        {
            if (GUILayout.Button("C# Configuration", _headerStyle))
            {
                Application.OpenURL("https://developers.google.com/protocol-buffers/docs/csharptutorial#defining-your-protocol-format");
            }
            
            EditorGUILayout.BeginVertical("box");

            _creation.csharpNamespace = DrawTextField("C# Namespace", _creation.csharpNamespace);
            if (string.IsNullOrEmpty(_creation.csharpNamespace))
            {
                GUILayout.Label("Warning: Proto Package will take over to generate C# Namespace", _warningStyle);
            }

            EditorGUILayout.EndVertical();
        }

        private void DrawJava()
        {
            if (GUILayout.Button("Java Configuration", _headerStyle))
            {
                Application.OpenURL("https://developers.google.com/protocol-buffers/docs/javatutorial#defining-your-protocol-format");
            }
            
            EditorGUILayout.BeginVertical("box");

            _creation.javaMultipleFiles = EditorGUILayout.Toggle("Multiple Files", _creation.javaMultipleFiles);
            _creation.javaPackage       = DrawTextField("Java Package", _creation.javaPackage);
            if (string.IsNullOrEmpty(_creation.javaPackage))
            {
                GUILayout.Label("Warning: Proto Package will take over to generate Java Package", _warningStyle);
            }

            EditorGUILayout.EndVertical();
        }

        private void DrawBrowsePath()
        {
            EditorGUILayout.BeginVertical("box");
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Save Location", _headerStyle))
            {
                if (Directory.Exists(_creation.storeLocation))
                {
                    Application.OpenURL(_creation.storeLocation);
                }
                else
                {
                    Application.OpenURL(Application.dataPath);
                }
            }
            
            _creation.storeLocation = GUILayout.TextField(_creation.storeLocation, GUILayout.Width(435));

            EditorGUILayout.EndHorizontal();
            GUILayout.Space(5);
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Browse", GUILayout.Width(100)))
            {
                string selectedPath = EditorUtility.OpenFolderPanel("Location to save", _creation.storeLocation, "");
                if (!string.IsNullOrEmpty(selectedPath))
                {
                    _creation.storeLocation = selectedPath;
                }

                // remove all focus
                GUI.FocusControl(null);
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        private void DrawExecute()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Execute", GUILayout.Width(100), GUILayout.Height(40)))
            {
                Execute();
                Save();

                // remove all focus
                GUI.FocusControl(null);
            }

            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();
        }

        private void Save() { ProtoEditorServices.StoreObject(CreationKey, _creation); }

        private void Execute()
        {
            if (Directory.Exists(_creation.storeLocation))
            {
                if (ValidateInput())
                {
                    Create();
                }
            }
            else
            {
                EditorUtility.DisplayDialog("Error", "Save Location is invalid", "OK");
            }
        }

        private bool ValidateInput()
        {
            bool isValid = true;

            isValid &= !string.IsNullOrEmpty(_fileName);
            if (!isValid)
            {
                EditorUtility.DisplayDialog("Error", "File Name is empty. Please fill up", "OK");
            }

            return isValid;
        }

        private void Create()
        {
            string finalName        = _fileName.Trim().ToLower().Replace(".proto", "").Replace(" ", "_");
            string messageClassName = GetMessageClassName(finalName);
            
            string finalProtoPackage = _creation.protoPackage;
            if (!string.IsNullOrEmpty(finalProtoPackage))
            {
                finalProtoPackage = finalProtoPackage.Trim().Replace(" ", ".").ToLower();
            }
            
            string finalCSharpNamespace = _creation.csharpNamespace;
            if (!string.IsNullOrEmpty(finalCSharpNamespace))
            {
                finalCSharpNamespace = finalCSharpNamespace.Trim().Replace(" ", ".");
            }
            
            string finalJavaPackage = _creation.javaPackage;
            if (!string.IsNullOrEmpty(finalJavaPackage))
            {
                finalJavaPackage = finalJavaPackage.Trim().Replace(" ", ".").ToLower();
            }

            string fileContent = MessageBody;
            if (!string.IsNullOrEmpty(finalProtoPackage))
            {
                string protoPackage = ProtoPackageBody.Replace("**Package**", finalProtoPackage);
                fileContent = fileContent.Replace("**ProtoPackage**", protoPackage);
            }
            else
            {
                fileContent = fileContent.Replace("**ProtoPackage**", String.Empty);
            }
            
            if (!string.IsNullOrEmpty(finalCSharpNamespace))
            {
                string csharpNamespace = ProtoOptionBody.Replace("**OptionParam**", "csharp_namespace");
                csharpNamespace = csharpNamespace.Replace("**OptionValue**", finalCSharpNamespace);
                fileContent     = fileContent.Replace("**CSharp**", csharpNamespace);
            }
            else
            {
                fileContent = fileContent.Replace("**CSharp**", String.Empty);
            }

            string javaMultipleFile = ProtoOptionBody.Replace("**OptionParam**", "java_multiple_files");
            javaMultipleFile = javaMultipleFile.Replace("**OptionValue**", _creation.javaMultipleFiles.ToString().ToLower()).Replace("\"", "");

            string java = javaMultipleFile + "\n";
            if (!string.IsNullOrEmpty(finalJavaPackage))
            {
                string javaPackage = ProtoOptionBody.Replace("**OptionParam**", "java_package");
                javaPackage =  javaPackage.Replace("**OptionValue**", finalJavaPackage);
                java        += javaPackage + "\n";
            }
            
            if (_creation.javaMultipleFiles)
            {
                string finalJavaOuterClass = messageClassName + "OuterClass";
                string javaOuterClass      = ProtoOptionBody.Replace("**OptionParam**", "java_outer_classname");
                javaOuterClass =  javaOuterClass.Replace("**OptionValue**", finalJavaOuterClass);
                java           += javaOuterClass;
            }
            
            fileContent = fileContent.Replace("**Java**", java);
            
            fileContent = fileContent.Replace("**ClassName**", messageClassName);

            string finalPath = _creation.storeLocation + "\\" + finalName + ".proto";
            using (StreamWriter sw = new StreamWriter(finalPath, false))
            {
                sw.Write(fileContent);
                sw.Dispose();
                sw.Close();

                if (EditorUtility.DisplayDialog("File Created", "Jump to stored location", "OK", "Cancel"))
                {
                    Application.OpenURL(_creation.storeLocation);
                }
            }
        }

        private string GetMessageClassName(string theName)
        {
            string newName = String.Empty;
            var    arr     = theName.Split('_');
            foreach (var str in arr)
            {
                var charArr = str.ToCharArray();
                for (int i = 0; i < charArr.Length; i++)
                {
                    int c = charArr[i];
                    if (i == 0 && c > 90) 
                        c -= 32;
                    else if (i > 0 && c < 90)
                    {
                        c += 32;
                    }

                    charArr[i] = (char)c;
                }

                newName += new string(charArr);
            }

            return newName;
        }
    }
}