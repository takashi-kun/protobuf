﻿namespace Proto.Testing
{
    using System;
    using System.IO;
    using Google.Protobuf;
    using Proto.Template.Models;
    using UnityEngine;
    
    public class MonoTesting : MonoBehaviour
    {
        private UserData _userData;

        [SerializeField]
        private string _fileNameToWrite = "user_data.bin";
        
        [SerializeField]
        private string _fileNameToRead = "user_data.dat";

        private string _path;
        
        private void Start()
        {
            InitData();
        }

        private void InitData()
        {
            _path = Application.dataPath + @"/Stored/";
            
            _userData = new UserData()
            {
                UserName = "takashii",
                Level    = 100,
                People =
                {
                    new UserInfo()
                    {
                        Email = @"kingdom@hotmail.com",
                        Id    = 1,
                        Name  = "master"
                    },
                    new UserInfo()
                    {
                        Email = @"thatWhatSeaSaid@hotmail.com",
                        Id    = 2,
                        Name  = "main"
                    }
                }
            };
        }


        public void WriteFile()
        {
            if (!Directory.Exists(_path))
            {
                Directory.CreateDirectory(_path);
            }
            
            using (var output = File.Create(_path + _fileNameToWrite))
            {
                _userData.WriteTo(output);
                output.Dispose();
                output.Close();
            }
        }

        public void ReadData()
        {
            using (var input = File.OpenRead(_path + _fileNameToRead))
            {
                UserData temp = UserData.Parser.ParseFrom(input);
                if (temp != null)
                {
                    Debug.Log(temp.ToString());
                }
            }
        }
    }
}