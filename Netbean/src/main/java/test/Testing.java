/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

//import com.protobuf.model.UserData;
//import com.protobuf.model.UserInfo;
//import com.protobuf.model.UserInfoOrBuilder;
//import com.protobuf.model.UserInfoOuter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author raphael
 */
public class Testing {
    
    private static String ReadFileName = "user_data.bin";
    private static String StoredFileName = "user_data.dat";
    
    public static void main(String[] args) throws IOException {
        Path currentRelativePath = Paths.get("");
        String currentPath = currentRelativePath.toAbsolutePath().toString();
        
        String fileToRead = currentPath.concat("/stored/").concat(ReadFileName);
        String fileToWrite = currentPath.concat("/stored/").concat(StoredFileName);
        
        File fileRead = new File(fileToRead);
        if (fileRead.exists() && !fileRead.isDirectory()){
            System.out.println("Start reading...");
            Read(fileToRead);
        }
        else{
            System.out.println("There're no file to read!");
        }
        
        System.out.println("Start writing...");
        Write(fileToWrite);
        System.out.println("....");
    }
    
    private static void Read(String path){
        try {
            var fileInputStream = new FileInputStream(path);
//            UserData userData = UserData.parseFrom(fileInputStream);
//            System.out.println(userData.toString());

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    private static void Write(String path) throws IOException{
//        try {
//            UserData.Builder userData = UserData.newBuilder();
//            userData.setUserName("magician");
//            userData.setLevel(100000);
//            
//            UserInfo.Builder info = UserInfo.newBuilder();
//            info.setEmail("endlesslove@yahoo.com");
//            info.setId(7);
//            info.setName("dopamine");
//            //go on
//            
//            userData.addPeople(info);
//  
//            FileOutputStream output = new FileOutputStream(new File(path));
//            userData.build().writeTo(output);
//            output.close();
            
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Testing.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
}
